{
  hostname_prefix = 'ffc-',

  site_name = 'Freifunk Chemnitz',

  site_code = 'ffc',

  default_domain = 'chemnitz',

  timezone = 'CET-1CEST,M3.5.0,M10.5.0/3',

  ntp_servers = { '2.de.pool.ntp.org' },

  regdom = 'DE',

  wifi24 = {
	mesh = {
		mcast_rate = 12000,
    },
  },

  wifi5 = {
	mesh = {
		mcast_rate = 12000,
        outdoor = false,
    },
  },

  mesh_vpn = {
    enabled = true,

    fastd = {
      methods = {'null@l2tp','null', 'salsa2012+umac'},
      mtu = 1426,
      configurable = true,

      bandwidth_limit = {
        enabled = false,

        egress = 1000,

        ingress = 20000,
      },
    },
  },

  autoupdater = {
    branches = {
      stable = {
        name = 'stable',

        mirrors = {
          'http://firmware.dev.ffc/stable/sysupgrade',
          'http://firmware.chemnitz.freifunk.net/stable/sysupgrade',
          'http://gianotti.chemnitz.freifunk.net/stable/sysupgrade',
          'http://[2001:bc8:3f13:100::1]/stable/sysupgrade'
        },

        good_signatures = 1,

        pubkeys = {
          'e09f23bec96388411a804a2df508fa137653470bf6c046ed41c6df916150d689', -- olorenz
          'f22b4be67cd2a3ae53bcf442252f0f203401255217f26b29effc1f7d9e9d72ff', -- nemesis
          '39358e086138b0e671ad007fbd2f0805560f1bad4b63549e1441d0b4f80b8737', -- bequerel (server)
        },
      },
      experimental = {
        name = 'experimental',

        mirrors = {
          'http://firmware.dev.ffc/experimental/sysupgrade',
          'http://firmware.chemnitz.freifunk.net/experimental/sysupgrade',
          'http://gianotti.chemnitz.freifunk.net/experimental/sysupgrade',
          'http://[2001:bc8:3f13:100::1]/experimental/sysupgrade'
        },

        good_signatures = 1,

        pubkeys = {
          'e09f23bec96388411a804a2df508fa137653470bf6c046ed41c6df916150d689', -- olorenz
          'f22b4be67cd2a3ae53bcf442252f0f203401255217f26b29effc1f7d9e9d72ff', -- nemesis
          '39358e086138b0e671ad007fbd2f0805560f1bad4b63549e1441d0b4f80b8737', -- bequerel (server)
        },
      },
    },
  },

  mesh = {
    batman_adv = {
      gw_sel_class = 7,
    },
  },

	authorized_keys = {
    'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCtBZ2uUnUzOeJTuGB/EYBOxhX9o7q0Y3GlTgPHflE07CVNnf4ElPr3a1Y3J1oQm/tfDX1SAEPfSP38GYw6BcFcnn0/garIrHC8pUG2GINrgROwLvAE/fojICbAzq7k4P97ZKLOysig2WS1bKg3zRoOODgnPM8Sl3ymd+MA3XrPUDDimJxOiPL0hSZ3fDTvo5y2GNVZSLb7C3UZ8Ul2ONu/6nqv5sZnCHNJ5QbNeXjKsoPCH4wPwF6P2VW4i5mbKIldL82RkHP/9HbMtCCb0RSIAiOI47g0438MkHgEIvPpfqaKNoMqev4FQJNAOIoyD17zo8guyjU9ViouPAMPErAKo/t+4A/t6RVSegAe5rwWPgUauGkohIVcuXXwzY1YFCQYPw/Bq4ndxmQiu8k44CtW1ZacOEBp8XoUg/9bZyRJ2YDSaWTQnc9yddG22CyD+bCB9FNq2yoI1t+2trdqsCDBXxgQ3ab3XuEG+q39E0BXqlyg5TUTvQkpuC5A1pFyi8OqcxjRdqSIxpQOgXcUvP69d/SWa4pMRimJn3mXAksTTILxaHyKzS4pgkyS/0x6M5SQyiS0DFtZRbqvMm1aylNNLAL13jaarl1//94eR6Aj8IZTFiaPg5RnotazsbZNQ4olY8PktIoKh7SL+ILuKGA85Gn32rFUOI2Jio3JwdPetw== nemesis',
		'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCdqC/SFYYhXdykKJbgH3OuROXIXeOSCOulwB17bk9hUVVFGZcKTfNSSR60uQa8e8SK/Z2E8iOUGqLHVn8hJXnbj0ibi9MyuCrYKKWAi7VXZbUk6WhG22dspnQhEftsb/DSrNpiD0NWycsK8fz5BLVCbNcjpoLj9MTesNXHPmwdn2Jxo3Fp6AEDtnjO5k2vldJzO3qET9BB8BFQoq1DsQO2SMuWi4l1gQ+YIpPyNelwxyYoRLhQuET3rJABhyKvw72A6HM9NTW5nf8weMdyeePmENY0kgpkhrfjxJEzi9JGQVSOeFkoDxK5AKnIIcAWNnRa6fMdsf03/Q2TWSKt+Fz+YA6PBuyE7U1yXowjLCWOJYV7C2T3EQ+l6gk0xTckAAN0A/xXwyDj2bbjL6Ue452dRw3fTYu1CHZ7jnyDOjs5MHjFKE+ka7yQtpWsKKfVpCG8U5uGLS6zELEE04TCChKFP1XL1yV6n4DU9EMwU6je2ujXAZNEuWXwiw2SPep7jTyHz+bRntGIfhuu4p/85xzNR14MLTd/+yM8hc+ESzJeitX8OU6BpQXLdtA+CA6mdxWXNTBZ9Zu2EG7eYXJ37fwJBLeEFaVc+/JtOxEiPj3sOiitoPgBxMYgNVr9bKw1ld3NJwecDQe9d/Yz9HnIN7jFvoURBOfBjjvfOpA439xGhw== txt@txt300s',
		'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAAEAQDwK6/HhcohHm0DxLP7E6QjaHIYPp5yejosOleBqDlHJDH2Vrewdu21O94ysn57B6dHAA4TwPHJG3oH2X7rrPRalKMcyx0QCo2VKvP0ZAjCKTei/25I8Dm6buwwjLODV8oDn/4oCR4efkdbGEFSD8mWF1Nr2xLu/1dPqHo7UZ2OI/pD7z1pPqZkiA6M3f91GuauuXaqQ7i/LW8bsprF4Cj6eK7RGqol2TUvH0q0tJL+5oFw2QJUIyRnbNXWMjQ/20wFIGK/tn8ntd4kzWDjdbqONTONC2DCu/+huFSBDf11k79dVBXod6Vds60+RwkuUxYKDauJZCH4yaHsQ5FzzoRhlXaLUWHJmBJzX9XuTVFYfdkuHZwvBbpcuf2kTHyrBwKSr407HicGRzieX5FvTURBC95m1hTRagYKfHzL/jMJ231E02QgeuOBtMp4BamLr+lPe8A/nek6onBGsMQj1GCBgMaAG6QNMKod91RdAr4wNMX+dxE99NFxTxIiUUYT4s4eLTcN1EaHKBFFDJqX4WHUqyOhooI7iYWHGmsxV9XDjiQNYniX6c5ds4crXfDKByciK2vxW9+nx80jnwnsZXxOxPFVy3+SkeFia98Y/uJ6umsMkJXBEN45PTaIdlqBktoA/dZY9Hdec5uU8MI4Wtg3nDXIphxMNnWqh4bXMfTyOY4VgmaeReEf5KAq+8vy8p84utXPLwN+0RINsiD3NSY2SppLFRB35gf8YM/jBI0mP+UMZ5WdtIhAXgD3l0yzybFtO1JD/4C7dmcKpbyRHI+jCherZG1npQaT4567/jfctPrxlik1NpGP1ltvgx9HGKBmdkU7n5AHwZA4EHNdIJoZE4VNp2D95CufybGIlIIlZoL6hoHOzLBxMJ5liX67iza0Av4QCWZZKO2YLTPEOQcW+CUO8CcSO7gX8D3J72kUXCbXzBj2SKDAoqcynhYyIr2bE6rzjcppipHcZwTuZG31yFu58dhcdBTLCvpWXH5GFGpINgUIq/ij6KFNEQarflSQohXdhgZstiMsAZs5HCLxeTrxPEkfimrMU6ScQ3wMC1NlaKkFjnrzOfw3J8OwAjE8QWdOBeONLxF/DN4LRWHJNf7P9RhMDyEEYxG+rGCNk/9/6Q7LZGnb4RC7PJvPp3CB2i4KmIFKSAZQ2wZcmJmwufpIxlzN8kPmxI21V6CUdx8G/qRl0xtVmFjOzBFOt87Gf3HyWGYcKXjFMzfk5vnEKtyPFVAYC6UX7bvPSmpNTqVJWpCuXFL8DbWgL083WSxTfXh3qONMinikeuEU2HbW8cVXmCzicwfI+E3g/DoapcDpA63g6GHApscvuj1j5spDfuA6+YdwMXm1xfMnXUFb data',
    'ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPW+Zv93OPjo5URQGrfJ5j6JQEJEW6nn7h0zn1rceIGT krapp@thinkpad',
    'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDMB+dDSbkFP30hCNwQ7v0om8eNUA+rOVmipOOqLj76J83QkdnbXHW7fQmR4lsK6ACPjriWs1ZPIcOAEzK/7eEIMbHiG3R9GszKC12JQr52X+t8kwgLsikRRP+IUq9xSDZeKOKU87nlLDOJjo2nK2OIQV2te7L0KRrudAEsx/ksrzUiuzNXXQdUCbcMkKDB76ZVKghPcGPn27iypCBpw87lMb8A5BrOQDx3rSERGFEiY/LjT1Hakd1SBLO6MDtNrzNhvsuurrYeQ4Nsr78q7PtFa3d1xIRu9R0HMh6SMeCpcTYeB5Vo6fcpib+ImWGCkVX5oCpJqK8IYJPjZ7Sg8mfF9O/sxcJxuLxA7G6bJgF5obgS7lOV3Jkl8LG/i9PiNb3b+9Zu+GGNp82eu3mGMChqb6PRAmphZM3iKnnCuEw7do6oLgKiPkeMIsgWq8+oZIC1CEZEq3ypLMDSe25qDbR/jTn0pIIMJ6kHXRGqU8voEj9Vycb3YwTjdtY+XsPRVHLBLk19U09aAwBXEcC4uNTG7EHJCwCKMlghP/0QZMU+uW32TTDaOcRsCEuwjN2IydidZu/zxhpkHsw4iGbISOX5qe4Cric++CJ9w2BXgI87eF/oySjvqm8Fyv58bGr2VXSa65oRjYzArcBx6jpXFCr6cm4rwiKpQdbFwt4UzEFtOQ== olorenz'
  },

	setup_mode = {
		skip = true,
	},

	config_mode = {
		geo_location = {
			show_altitude = false,
      osm = {
        center = {
          lat = 50.831666506,
          lon = 12.922472656,
        },
        zoom = 13,
      },
		},
	},

  roles = {
		default = 'node',
		list = {
			'node',
			'temporary',
			'backbone',
			'service',
		},
	},
}
